#TinEngine#
#### A lightweight extension platform for SpriteKit.####

SpriteKit is a great platform, and is pretty full-featured. There are some small holes in it though, and Tin hopes to fill those holes, or at least most of them :) Like the metal it is named after, Tin is lightweight and malleable – it isn't a full-featured engine on its own, but acts as an extension off of SpriteKit. 

* [Stage-centric.](https://bitbucket.org/tyskwo/tinengine/overview#markdown-stage-centric)
* [Motion system.](https://bitbucket.org/tyskwo/tinengine/overview#markdown-motion-system)
* [Extensions.](https://bitbucket.org/tyskwo/tinengine/overview#markdown-extensions)


http://tyskwo.com

- - -

## Stage-centric.

Tin takes inspiration from FlashPunk and other AS3- and Haxe-centric game libraries and uses a stage-centric design to organizing the game flow. In SpriteKit, there are SKScenes, and similarly to a finite state machine, can be transitioned between them. They are somewhat fickle, though, as there is not much control or customization of the transition, especially on a node-specific level. Tin takes the SKScene and creates a 'stage' reference to it. There are then TinScenes, which have transitionIn() and transitionOut() functions, allowing for more control when moving to and from different sections of the game: like from the main menu to the gameplay for instance. 
 
## Motion system.

SKActions are great, and are a great base to code-based animations, but are still very limited. There is a slim selection of timing functions, and generally to get a single group of actions together takes upwards of 15 lines of code. Tin fixes both of those issues with TinTimingEffects, TinEffects, and TinTween. TinTimingEffects has the entirety of standard timing functions: everything from Exponential, to Elastic and Bounce, all in the In, Out, and InOut variety. TinEffects use these timing functions as a substitute to SKActions. (Right now there is fade, rotate, move, and scale). TinTweens then take these TinEffects and wrap them in a nice single function call, with a start and end condition, duration, and delay.

## Extensions.

On the smaller scale, throughout my time developing in SpriteKit, I've collected some useful extensions to many of the basic data types – like clamping, operator overloading, random values, linear lerping etc. Some more specialized ones for SKNodes include afterDelay(), scaleAsPoint(), and bringToFront().



That's it for now, but I plan on keep updating this as I continue down this road.
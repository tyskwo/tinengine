import SpriteKit

public extension SKNode
{
    //Treat the node's scale as a CGPoint value.
    public var scaleAsPoint: CGPoint
    {
        get { return CGPoint(x: xScale, y: yScale) }
        set
        {
            xScale = newValue.x
            yScale = newValue.y
        }
    }
    
    //Runs an action on the node that performs a closure or function after a given time.
    public func afterDelay(delay: NSTimeInterval, runBlock block: dispatch_block_t)
        { runAction(SKAction.sequence([SKAction.waitForDuration(delay), SKAction.runBlock(block)])) }
    
    //Makes this node the frontmost node in its parent.
    public func bringToFront()
    {
        if let parent = self.parent
        {
            removeFromParent()
            parent.addChild(self)
        }
    }
}
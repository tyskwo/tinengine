import CoreGraphics
import SpriteKit

public extension CGVector {
    //Creates a new CGVector given a CGVector.
    public init(vector: CGVector) { self.init(dx: vector.dx, dy: vector.dy) }
    
    //Given an angle in radians, creates a vector of length 1.0 and returns the result as a new CGVector. An angle of 0 is assumed to point to the right.
    public init(angle: CGFloat) { self.init(dx: cos(angle), dy: sin(angle)) }
    
    //Adds (dx, dy) to the vector.
    public mutating func offset(dx dx: CGFloat, dy: CGFloat) -> CGVector
    {
        self.dx += dx
        self.dy += dy
        
        return self
    }
    
    //Returns the length (magnitude) of the vector described by the CGVector.
    public func length() -> CGFloat
        { return sqrt(dx*dx + dy*dy) }
    
    public func lengthSquared() -> CGFloat
        { return dx*dx + dy*dy }
    
    //Normalizes the vector described by the CGVector to length 1.0 and returns the result as a new CGVector.
    public func normalized() -> CGVector
    {
        let l = length()
        return l > 0 ? self / l : CGVector.zero
    }
    
    //Normalizes the vector described by the CGVector to length 1.0.
    public mutating func normalize() -> CGVector
    {
        self = normalized()
        return self
    }
    
    //Calculates the distance between two CGVectors.
    public func distanceTo(vector: CGVector) -> CGFloat
        { return (self - vector).length() }
}


//************************************************************************************************************************
//operator overloads
//************************************************************************************************************************



//Adds two CGVector values and returns the result as a new CGVector.
public func + (left: CGVector, right: CGVector) -> CGVector
    { return CGVector(dx: left.dx + right.dx, dy: left.dy + right.dy) }



//Increments a CGVector with the value of another.
public func += (inout left: CGVector, right: CGVector)
    { left = left + right }



//Subtracts two CGVector values and returns the result as a new CGVector.
public func - (left: CGVector, right: CGVector) -> CGVector
    { return CGVector(dx: left.dx - right.dx, dy: left.dy - right.dy) }



//Decrements a CGVector with the value of another.
public func -= (inout left: CGVector, right: CGVector)
    { left = left - right }



//Multiplies two CGVector values and returns the result as a new CGVector.
public func * (left: CGVector, right: CGVector) -> CGVector
    { return CGVector(dx: left.dx * right.dx, dy: left.dy * right.dy) }

//Multiplies the x and y fields of a CGVector with the same scalar value and returns the result as a new CGVector.
public func * (point: CGVector, scalar: CGFloat) -> CGVector
    { return CGVector(dx: point.dx * scalar, dy: point.dy * scalar) }



//Multiplies a CGVector with another.
public func *= (inout left: CGVector, right: CGVector)
    { left = left * right }

//Multiplies the x and y fields of a CGVector with the same scalar value.
public func *= (inout vector: CGVector, scalar: CGFloat)
    { vector = vector * scalar }



//Divides two CGVector values and returns the result as a new CGVector.
public func / (left: CGVector, right: CGVector) -> CGVector
    { return CGVector(dx: left.dx / right.dx, dy: left.dy / right.dy) }

//Divides the x and y fields of a CGVector by the same scalar value and returns the result as a new CGVector.
public func / (vector: CGVector, scalar: CGFloat) -> CGVector
    { return CGVector(dx: vector.dx / scalar, dy: vector.dy / scalar) }



//Divides a CGVector by another.
public func /= (inout left: CGVector, right: CGVector)
    { left = left / right }

//Divides the x and y fields of a CGVector by the same scalar value.
public func /= (inout vector: CGVector, scalar: CGFloat)
    { vector = vector / scalar }




//Performs a linear interpolation between two CGVector values.
public func lerp(start start: CGVector, end: CGVector, t: CGFloat) -> CGVector
    { return start + (end - start) * t }

import SpriteKit

/**
 * Allows actions with custom timing functions.
 *
 * Unfortunately, SKAction does not utilize timing function, so
 * we need to replicate the actions using SKTEffect subclasses.
 */
public class TinEffect
{
    unowned var node:           SKNode
            var duration:       NSTimeInterval
            //var delta:          CGFloat
            //var delta:          CGPoint
    public  var timingFunction: ((CGFloat) -> CGFloat)?
    
    public init(node: SKNode, duration: NSTimeInterval)
    {
        self.node = node
        self.duration = duration
        timingFunction = TimingFunctionLinear
    }
    
    //subclasses implement this
    public func update(t: CGFloat) {}
}

//Fades a node to a certain alpha.
public class FadeEffect: TinEffect
{
    var startAlpha:    CGFloat
    var delta:         CGFloat
    var previousAlpha: CGFloat
    
    public init(node: SKNode, duration: NSTimeInterval, endAlpha: CGFloat)
    {
        previousAlpha   = node.alpha
        self.startAlpha = node.alpha
        
        delta = endAlpha - startAlpha
        
        super.init(node: node, duration: duration)
    }
    
    public override func update(t: CGFloat)
    {
        let newAlpha  = startAlpha + delta*t
        previousAlpha = newAlpha
        
        node.alpha    = newAlpha
    }
}

//Moves a node from its current position to a new position.
public class MoveEffect: TinEffect
{
    var startPosition:    CGPoint
    var delta:            CGPoint
    var previousPosition: CGPoint
    
    public init(node: SKNode, duration: NSTimeInterval, startPosition: CGPoint, endPosition: CGPoint)
    {
        previousPosition   = node.position
        self.startPosition = startPosition
        
        delta = endPosition - startPosition
        
        super.init(node: node, duration: duration)
    }
    
    public override func update(t: CGFloat)
    {
        //Allows multiple MoveEffect objects to modify the same node concurrently
        let newPosition  = startPosition + (delta * t)
        let difference   = newPosition - previousPosition
        
        previousPosition = newPosition
        
        node.position    += difference
    }
}

//Scales a node to a certain scale factor.
public class ScaleEffect: TinEffect
{
    var startScale:    CGPoint
    var delta:         CGPoint
    var previousScale: CGPoint
    
    public init(node: SKNode, duration: NSTimeInterval, startScale: CGPoint, endScale: CGPoint)
    {
        previousScale   = CGPoint(x: node.xScale, y: node.yScale)
        self.startScale = startScale
        
        delta = endScale - startScale
        
        super.init(node: node, duration: duration)
    }
    
    public override func update(t: CGFloat)
    {
        let newScale   = startScale + (delta * t)
        let difference = newScale / previousScale
        
        previousScale  = newScale
        
        node.xScale    *= difference.x
        node.yScale    *= difference.y
    }
}

//Rotates a node to a certain angle.
public class RotateEffect: TinEffect
{
    var startAngle:    CGFloat
    var previousAngle: CGFloat
    var delta: CGFloat
    
    public init(node: SKNode, duration: NSTimeInterval, startAngle: CGFloat, endAngle: CGFloat)
    {
        previousAngle   = node.zRotation
        self.startAngle = startAngle
        
        delta = endAngle - startAngle
        
        super.init(node: node, duration: duration)
    }
    
    public override func update(t: CGFloat)
    {
        let newAngle   = startAngle + (delta * t)
        let difference = newAngle - previousAngle
        
        previousAngle  = newAngle
        
        node.zRotation += difference
    }
}



//Extension that allows SKTEffect objects as regular SKActions.
public extension SKAction
{
    public class func actionWithEffect(effect: TinEffect) -> SKAction
    {
        return SKAction.customActionWithDuration(effect.duration)
        { node, elapsedTime in
            
            var t = elapsedTime / CGFloat(effect.duration)
            
            if let timingFunction = effect.timingFunction
            {
                t = timingFunction(t)
            }
            
            effect.update(t)
        }
    }
}

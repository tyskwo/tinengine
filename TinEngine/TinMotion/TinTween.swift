import SpriteKit

struct TinTween
{
    //function to fade node with given effect, delay, duration, and end alpha value
    static func addFadeEffect(node: SKNode, effect: (CGFloat) -> CGFloat, delay: NSTimeInterval, duration:NSTimeInterval, endAlpha:CGFloat)
    {
        let fadeEffect = FadeEffect(node: node, duration: duration, endAlpha: endAlpha)
        fadeEffect.timingFunction = effect
        
        node.runAction(SKAction.afterDelay(delay, performAction: SKAction.actionWithEffect(fadeEffect)))
    }
    
    //function to move node with given easing function, delay, duration, start position and end position
    static func addMoveEffect(node: SKNode, effect: (CGFloat) -> CGFloat, delay: NSTimeInterval, duration:NSTimeInterval, startPos: CGPoint, endPos: CGPoint)
    {
        let moveEffect = MoveEffect(node: node, duration: duration, startPosition: startPos, endPosition: endPos)
        moveEffect.timingFunction = effect
        
        node.runAction(SKAction.afterDelay(delay, performAction: SKAction.actionWithEffect(moveEffect)))
    }
    
    //function to rotate node with given easing function, delay, duration, start angle and end angle
    static func addRotateEffect(node: SKNode, effect: (CGFloat) -> CGFloat, delay: NSTimeInterval, duration:NSTimeInterval, startAngle: CGFloat, endAngle: CGFloat)
    {
        let rotateEffect = RotateEffect(node: node, duration: duration, startAngle: startAngle, endAngle: endAngle)
        rotateEffect.timingFunction = effect
        
        node.runAction(SKAction.afterDelay(delay, performAction: SKAction.actionWithEffect(rotateEffect)))
    }
    
    //function to scale node with given easing function, delay, duration, start scale and end scale
    static func addScaleEffect(node: SKNode, effect: (CGFloat) -> CGFloat, delay: NSTimeInterval, duration:NSTimeInterval, startScale: CGPoint, endScale: CGPoint)
    {
        let scaleEffect = ScaleEffect(node: node, duration: duration, startScale: startScale, endScale: endScale)
        scaleEffect.timingFunction = effect
        
        node.runAction(SKAction.afterDelay(delay, performAction: SKAction.actionWithEffect(scaleEffect)))
    }
}

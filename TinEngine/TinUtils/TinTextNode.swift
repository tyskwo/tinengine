import SpriteKit

//wrapper class for SKLabelNode
class TinTextNode: SKLabelNode
{
    //empty initializer
    override init() { super.init() }
    
    //init with given text, position, font size, color and name
    init(text:String, position: CGPoint, fontSize: CGFloat, fontColor: SKColor, fontName:String)
    {
        super.init(fontNamed: fontName)
        
        self.text      = text
        self.position  = position
        self.fontSize  = fontSize
        self.fontColor = fontColor
        
        //set alignment to center for both vertical and horizontal
        self.verticalAlignmentMode   = .Center
        self.horizontalAlignmentMode = .Center
    }
    
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    //getters for height and width without need to reference frame.
    func getHeight() -> CGFloat { return self.frame.height }
    func getWidth()  -> CGFloat { return self.frame.width  }
}


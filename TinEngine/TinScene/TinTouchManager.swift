import SpriteKit

struct TinTouch
{
    let touch:UITouch
    
    let beginPosition:CGPoint
    var endPosition  :CGPoint?
    
    init(touch:UITouch)
    {
        self.touch = touch
        beginPosition = touch.locationInNode(Stage.stageRef)
    }
}

class TinTouchManager
{
    var touchesList:[TinTouch]
    
    /*var totalDelta:CGFloat, totalDeltaX:CGFloat, totalDeltaY:CGFloat
    var largestDeltaX:CGFloat, largestDeltaY:CGFloat
    var averagePressure:CGFloat, largestPressure:CGFloat*/
    
    init()
    {
        touchesList = []
    }
    
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }

    
    
    
    func update() {}
    
    func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        if let touch = touches.first
            { touchesList.append(TinTouch(touch: touch)) }
    }
    
    func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {}
    
    func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        if let touch = touches.first
        {
            for var i = 0; i < touchesList.count; i++
            {
                if(touch === touchesList[i].touch)
                {
                    touchesList[i].endPosition = getAbsolutePositionOfTouch(touch)
                    
                    touchesList.removeAtIndex(i)
                    break
                }
            }
        }
    }
    
    func getNumberOfTouches() -> Int
        { return touchesList.count }
    
    func getAbsolutePositionOfTouch(touch: UITouch) -> CGPoint
        { return touch.locationInNode(Stage.stageRef) }
    
    func getNodeUnderTouch(touch: UITouch) -> SKNode
        { return Stage.stageRef.nodeAtPoint(getAbsolutePositionOfTouch(touch)) }
    
    func getOldestTouch() -> UITouch
    {
        var returnValue = touchesList[0].touch
        
        for touchStruct in touchesList
        {
            if(touchStruct.touch.timestamp < returnValue.timestamp)
                { returnValue = touchStruct.touch }
        }
        
        return returnValue
    }
    
    func getNewestTouch() -> UITouch
    {
        var returnValue = touchesList[0].touch
        
        for touchStruct in touchesList
        {
            if(touchStruct.touch.timestamp < returnValue.timestamp)
                { returnValue = touchStruct.touch }
        }
        
        return returnValue
    }

    func checkForNodeWithName(name: String) -> Bool
    {
        for touchStruct in touchesList
        {
            if(getNodeUnderTouch(touchStruct.touch).name! == name)
                { return true }
        }
        
        return false
    }
}


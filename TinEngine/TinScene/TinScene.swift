import SpriteKit

enum Scene
{
    case NULL,
         MainMenu,
         Game,
         PauseMenu,
         WinMenu
}

class TinScene
{
    //bool for whether or not a button in the menu was pressed
    var selected:Bool = false
    
    //the scene to transition to
    var selection:Scene = Scene.NULL
    
    //if the scene is currently transitioning
    var isMoving:Bool = false
    
    init() { selected = false }
    
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    func update() {}
    func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {}
    func touchesMoved(touches: Set<NSObject>, withEvent event: UIEvent) {}
    func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {}
    
    func transitionIn()  {}
    func transitionOut() {}
    
    func clear() { Stage.stageRef.removeAllChildren() }
}
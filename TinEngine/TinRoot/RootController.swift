import UIKit
import SpriteKit

class RootController: UIViewController
{
    //when applicaiton is loaded, on view loading
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // Configure the view.
        let view = self.view as! SKView
        
        /* Sprite Kit applies additional optimizations to improve rendering performance */
        view.ignoresSiblingOrder = true
        
        showDebugInfo(view)
        
        checkDeviceType(view.bounds.size)
        setFontSizes()
        
        let scene = RootScene(size: view.bounds.size)
        
        
        scene.scaleMode = .ResizeFill
        scene.size      = view.bounds.size
        
        view.presentScene(scene)
    }
    
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask
    {
        return UIInterfaceOrientationMask.Portrait
    }
    
    override func prefersStatusBarHidden() -> Bool { return true  }
    override func shouldAutorotate()       -> Bool { return false }
    
    
    
    
    
    
    func showDebugInfo(view:SKView)
    {
        view.showsFPS = true
        //view.showsNodeCount = true
        //view.showsDrawCount = true
    }

    func checkDeviceType(size: CGSize)
    {
        if(size.height == 736) //6 Plus / 6sPlus
        {
            DeviceType.isPad = false
            DeviceType.assetSize = "_Plus"
        }
        
        if(size.height == 667) //6 / 6s
        {
            DeviceType.isPad = false
            DeviceType.assetSize = "_6"
        }
        
        if(size.height == 568) //5 / 5c / 5s
        {
            DeviceType.isPad = false
            DeviceType.assetSize = "_5"
        }
        
        if(size.height == 480) //4 / 4s
        {
            DeviceType.isPad = false
            DeviceType.assetSize = "_4"
        }
        
        if(size.height == 1024) //iPad 2
        {
            DeviceType.isPad = true
            DeviceType.assetSize = "_PadNon"
        }
        
        if(size.height == 2048) //other iPads ( - Pro)
        {
            DeviceType.isPad = true
            DeviceType.assetSize = "_PadRet"
        }
    }
    
    func setFontSizes()
    {
        if(DeviceType.isPad == true)
        {
            UI.Font.sizes.small = 32 * 2
            UI.Font.sizes.large = 64 * 2
        }
        else
        {
            UI.Font.sizes.small = 32
            UI.Font.sizes.large = 64
        }
    }
}
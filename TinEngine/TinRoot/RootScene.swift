import SpriteKit


class RootScene: SKScene
{
    //the current screen
    var currentScene:TinScene!
    
    override init(size:CGSize)
    {
        //get scene's width and height
        Stage.stageWidth  = size.width
        Stage.stageHeight = size.height
        
        //set current menu
        currentScene = TinScene()
        
        //super init
        super.init(size: CGSize())
        
        //set skscene reference and set background color
        Stage.stageRef = self
        Stage.stageRef.backgroundColor = UI.Colors.stageColor;
    }
    
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    
    //function called on a touch begin
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
        { currentScene.touchesBegan(touches, withEvent: event!) }
    
    //function called on a touch end
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?)
        { currentScene.touchesEnded(touches, withEvent: event!) }
    
    //called every frame
    override func update(currentTime: CFTimeInterval)
    {
        currentScene.update()
        
        checkForTransition()
    }
    
    func checkForTransition()
    {
        /*//if the scene is 'selected,' or marked to exit, update current menu
        if (currentScene.selected && currentScene is MainMenu)
        {
            currentScene = Game()
        }
        else if (currentScene.selected && currentScene is Game)
        {
            if (currentScene.selection == .WinMenu)
            {
                currentScene = EndMenu()
            }
            else if(currentScene.selection == .PauseMenu)
            {
                currentScene = PauseMenu()//currentMenu.pause? - no override. do it in game?
            }
        }
        else if (currentScene.selected && currentScene is PauseMenu)
        {
            if (currentScene.selection == .Game)
            {
                currentScene = Game()
            }
            else if(currentScene.selection == .MainMenu)
            {
                currentScene = MainMenu()
            }
        }
        else if (currentScene.selected && currentScene is EndMenu)
        {
            currentScene = MainMenu()
        }*/
    }
}

import SpriteKit

struct DeviceType
{
    //bool for iPad check, and asset size string for loading correct assets.
    static var isPad:Bool!
    static var assetSize:String!
}

struct Stage
{
    //skscene to add children and run actions
    static var stageRef:SKScene!
    
    //size of skscene
    static var stageWidth:CGFloat!, stageHeight:CGFloat!
}

struct UI
{
    struct Colors
    {
        static let stageColor:SKColor! = SKColor(r:250, g:250, b:250)
        static let text:SKColor!       = SKColor(r:40,  g:40,  b:40)
    }
    
    struct Font
    {
        struct sizes
        {
            //text sizes
            static var small:CGFloat!, large:CGFloat!
        }
        
        struct types
        {
            //font names (given by file info)
            static let title = "FONT NAME HERE"
        }
    }
    
    struct Layers
    {
        static var TOP:CGFloat! = 1000
        
        struct UI
        {
            static var top:CGFloat!    = 600
            static var bottom:CGFloat! = 500
        }
        struct Gameplay
        {
            static var top:CGFloat!    = 400
            static var bottom:CGFloat! = 300
        }
        struct Background
        {
            static var top:CGFloat!    = 100
            static var bottom:CGFloat! = 50
        }
        
        static var BOTTOM:CGFloat! = 0
    }
}